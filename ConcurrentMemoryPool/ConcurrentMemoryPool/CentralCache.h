#pragma once

#include "Common.h"

class CentralCache
{
public:
	// 获取对象地址
	static CentralCache* GetInstance()
	{
		return &_sInst;
	}

	// ThreadCache从CentralCache申请若干Object
	size_t FetchRangeObj(void*& start, void*& end, size_t n, size_t bytes);

	// 获取一个非空的Span
	Span* GetOneSpan(SpanList& spanList, size_t bytes);

	// ThreadCache释放若干Object到CentralCache的某个Span中
	void ReleaseListToSpans(void* start, size_t bytes);

private:
	SpanList _spanLists[NUM_CLASSES];

private: // 单例模式
	CentralCache() {}

	CentralCache(const CentralCache&) = delete;

	static CentralCache _sInst; // 创建对象
};