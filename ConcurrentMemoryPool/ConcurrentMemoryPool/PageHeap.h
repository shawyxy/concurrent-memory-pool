#pragma once

#include "Common.h"

class PageHeap
{
public:
	static PageHeap* GetInstance()
	{
		return &_sInst;
	}

	// 获取一个k页的span
	Span* NewSpan(size_t k);

	// 返回从Object到Span的映射
	Span* MapObjectToSpan(void* obj);

	// PageHeap回收空闲的Span，并合并相邻的Span
	void ReleaseSpanToPageHeap(Span* span);

public:
	std::mutex _pageMtx;

private:
	SpanList _spanLists[PH_MAX_PAGES];

	//std::unordered_map<PAGE_ID, Span*> _idSpanMap;
	TCMalloc_PageMap2<32 - PAGE_SHIFT> _idSpanMap;


	ObjectPool<Span> _spanPool;

	PageHeap() {}
	PageHeap(const PageHeap&) = delete;

	static PageHeap _sInst;
};