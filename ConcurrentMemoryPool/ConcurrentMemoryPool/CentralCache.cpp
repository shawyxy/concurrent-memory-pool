#include "CentralCache.h"
#include "PageHeap.h"

// 类外创建全局静态对象
CentralCache CentralCache::_sInst;
ObjectPool<Span> SpanList::_spanPool;

// ThreadCache从CentralCache申请若干Object
// start/end(输出型参数)：对象范围	n: Object的个数
// bytes: 所需内存的字节数
size_t CentralCache::FetchRangeObj(void*& start, void*& end, size_t n, size_t bytes)
{
	size_t index = SizeClass::Index(bytes); // 哈希桶下标
	_spanLists[index]._mtx.lock(); // 加桶锁

	// 在哈希桶中取一个非空的Span
	Span* span = GetOneSpan(_spanLists[index], bytes);
	assert(span && span->_objectsList); // Span及自由链表不为空

	// 在Span中取出n个Object
	// 如果不够取，则取整个Span中的Object
	// 输出型参数
	start = span->_objectsList;
	end = span->_objectsList;
	size_t actualNum = 1;
	while (NextObj(end) && n - 1)
	{
		end = NextObj(end); // 链表迭代
		n--;
		actualNum++;
	}
	// 将剩下的Object拼接回去
	span->_objectsList = NextObj(end);
	// 将分配出去的最后一个Object指向空，防止越界
	NextObj(end) = nullptr;
	// 更新这个Span的Object被分配数
	span->_usedCount += actualNum;

	_spanLists[index]._mtx.unlock(); // 解桶锁
	return actualNum;
}
// 获取一个非空的Span
// spanList: CentralCache的某个哈希桶		bytes: Object的字节数
Span* CentralCache::GetOneSpan(SpanList& spanList, size_t objBytes)
{
	// 尝试寻找一个非空的Span
	Span* it = spanList.Begin();
	while (it != spanList.End())
	{
		if (it->_objectsList != nullptr)
		{
			return it;
		}
		else
		{
			it = it->_next;
		}
	}

	spanList._mtx.unlock(); // 解桶锁
	PageHeap::GetInstance()->_pageMtx.lock(); // 加PageHeap锁
	// spanList是空的，向PageHeap申请(单位是页)
	PAGE_ID nPage = SizeClass::NumMovePage(objBytes); // 申请Span的页数
	Span* span = PageHeap::GetInstance()->NewSpan(nPage);
	span->_isUsed = true; // 将Span标记为正在被CentralCache使用
	span->_objSize = objBytes;
	PageHeap::GetInstance()->_pageMtx.unlock(); // 解PageHeap锁

	// Span的起始/终止地址和跨度
	char* start = (char*)(span->_pageId << PAGE_SHIFT); // 注意类型转换
	char* end = (char*)(start + (span->_nPage << PAGE_SHIFT));

	// 将Span切割成若干Object
	// 1. 将内存挂到FreeList上
	span->_objectsList = start;
	// 2. 切分Span为多个Object(单位是objBytes)
	// 让start向后走一步以迭代
	void* tail = span->_objectsList;
	start += objBytes;

	// 在[start, end]中构建FreeList
	while (start < end)
	{
		NextObj(tail) = start;
		tail = NextObj(tail);
		start += objBytes;
	}
	// 3. 将最后一个Object的next指针置空
	NextObj(tail) = nullptr;
	// 将划分好的Span挂到CentralCache的SpanList上
	spanList._mtx.lock(); // 加桶锁
	spanList.PushFront(span);

	return span;
}
// ThreadCache释放若干Object到CentralCache的某个Span中
// start: ThreadCache返回的一段内存的地址 bytes: 返回内存的字节数
void CentralCache::ReleaseListToSpans(void* start, size_t bytes)
{
	size_t index = SizeClass::Index(bytes);
	_spanLists[index]._mtx.lock(); // 加桶锁

	// 遍历还回来的Object
	void* obj = start;
	while (obj)
	{
		void* next = NextObj(obj);
		// 通过Object首地址得到映射的Span的地址
		Span* span = PageHeap::GetInstance()->MapObjectToSpan(obj);
		// 将Object头插到Span上
		NextObj(obj) = span->_objectsList;
		span->_objectsList = obj;
		// 更新Span记录被分配Object的计数器
		span->_usedCount--;

		// 这个Span当初管理的所有Object都被还了回来
		if (span->_usedCount == 0)
		{
			// 将Span从CentralCache的哈希桶取出，并还原信息
			_spanLists[index].Erase(span);
			span->_prev = nullptr;
			span->_next = nullptr;
			span->_objectsList = nullptr;

			// 解CentralCache的桶锁
			_spanLists[index]._mtx.unlock();

			// 交给PageHeap管理
			PageHeap::GetInstance()->_pageMtx.lock(); // 加PageHeap大锁
			PageHeap::GetInstance()->ReleaseSpanToPageHeap(span);
			PageHeap::GetInstance()->_pageMtx.unlock(); // 解PageHeap大锁

			// 加CentralCache的桶锁
			_spanLists[index]._mtx.lock();
		}
		obj = next;
	}

	_spanLists[index]._mtx.unlock(); // 解桶锁
}