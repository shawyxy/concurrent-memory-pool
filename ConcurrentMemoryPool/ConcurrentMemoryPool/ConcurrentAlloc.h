#pragma once

#include "Common.h"
#include "ThreadCache.h"
#include "PageHeap.h"
#include "ObjectPool.h"

static void* ConcurrentAlloc(size_t bytes)
{
	if (bytes > TC_MAX_BYTES) // 大于256KB的内存申请
	{
		size_t alignSize = SizeClass::RoundUp(bytes); // 按页对齐
		size_t k = alignSize >> PAGE_SHIFT; // 对齐后的页数

		PageHeap::GetInstance()->_pageMtx.lock(); // 访问PageHeap的Span，加锁
		Span* span = PageHeap::GetInstance()->NewSpan(k); // 由PageHeap分配
		span->_objSize = bytes; // 统计大于256KB的页
		PageHeap::GetInstance()->_pageMtx.unlock(); // 解锁

		return (void*)(span->_pageId << PAGE_SHIFT); // 返回内存地址
	}
	else
	{
		if (TLSThreadCache_ptr == nullptr)
		{
			//TLSThreadCache_ptr = new ThreadCache;
			static ObjectPool<ThreadCache> objectsPool;

			objectsPool._poolMtx.lock();
			TLSThreadCache_ptr = objectsPool.New();
			objectsPool._poolMtx.unlock();
		}

		// for test
		//std::cout << std::this_thread::get_id() << ":" << TLSThreadCache_ptr << std::endl;
		return TLSThreadCache_ptr->Allocate(bytes);
	}
}

static void ConcurrentFree(void* ptr)
{
	assert(ptr);

	// 查表找到内存属于哪个Span
	Span* span = PageHeap::GetInstance()->MapObjectToSpan(ptr);
	size_t size = span->_objSize; // Span管理的字节数

	if (size > TC_MAX_BYTES) // 大于256KB，直接还给PageHeap
	{
		PageHeap::GetInstance()->_pageMtx.lock();
		PageHeap::GetInstance()->ReleaseSpanToPageHeap(span);
		PageHeap::GetInstance()->_pageMtx.unlock();
	}
	else // 小于256KB，还给ThreadCache
	{
		assert(TLSThreadCache_ptr);
		return TLSThreadCache_ptr->Deallocate(ptr, size);
	}
}