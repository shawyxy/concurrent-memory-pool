#pragma once

#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <unordered_map>

#include <time.h>
#include <assert.h>

#include "PageMap.h"

// 页大小转换偏移，每页 2^13 Bytes，即 8 KB
static const size_t PAGE_SHIFT = 13;
// ThreadCache最大能分配的内存块大小
static const size_t TC_MAX_BYTES = 256 * 1024;
// ThreadCache和CentralCache中自由链表(哈希桶)的个数
static const size_t NUM_CLASSES = 208;
// PageHeap中哈希桶的个数
static const size_t PH_MAX_PAGES = 129;


// 条件编译
#ifdef _WIN64
typedef unsigned long long PAGE_ID;
#elif _WIN32
typedef unsigned int PAGE_ID;
#else
	// Linux
#endif

#ifdef _WIN32
#include <Windows.h>
#else
	// ...
#endif

// 向堆按页申请空间
inline static void* SystemAlloc(size_t kpage)
{
#ifdef _WIN32
	void* ptr = VirtualAlloc(0, kpage << PAGE_SHIFT, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
#else
	// Linux下brk mmap等
#endif
	if (ptr == nullptr)
		throw std::bad_alloc();
	return ptr;
}

// 直接将内存还给堆
inline static void SystemFree(void* ptr)
{
#ifdef _WIN32
	VirtualFree(ptr, 0, MEM_RELEASE);
#else
	// Linux下sbrk unmmap等
#endif
}

// 提取结点的指针部分
static void*& NextObj(void* ptr)
{
	return *(void**)ptr;
}

// 自由链表：管理相同大小的小内存块(不大于256KB)
//			SizeClass规格的Object
class FreeList
{
public:
	// 头插
	void Push(void* obj)
	{
		assert(obj);

		NextObj(obj) = _freeList_ptr;
		_freeList_ptr = obj;
		_size++;
	}
	// 头删
	void* Pop()
	{
		assert(_freeList_ptr); // 链表不为空

		void* ptr = _freeList_ptr;
		_freeList_ptr = NextObj(_freeList_ptr);

		_size--;

		return ptr;
	}
	// 插入一段范围的Object到自由链表
	// start/end：地址范围	n: Object个数
	void PushRange(void* start, void* end, size_t n)
	{
		assert(start);
		assert(end);

		// 头插
		NextObj(end) = _freeList_ptr;
		_freeList_ptr = start;
		_size += n;
	}
	// 从自由链表获取一段范围的Object
	// start/end：输出型参数
	void PopRange(void*& start, void*& end, size_t n)
	{
		assert(n <= _size);

		// 头删
		start = _freeList_ptr;
		end = _freeList_ptr;

		for (size_t i = 0; i < n - 1; i++)
		{
			end = NextObj(end);
		}

		_freeList_ptr = NextObj(end);
		NextObj(end) = nullptr;
		_size -= n;
	}

	bool Empty()
	{
		return _freeList_ptr == nullptr;
	}

	size_t Size()
	{
		return _size;
	}

	size_t& MaxSize()
	{
		return _maxSize;
	}
public:
	void* _freeList_ptr = nullptr; // 自由链表的起始地址
	size_t _size = 0; // 自由链表的结点个数
	size_t _maxSize = 1; // Object的最大个数
};

// 管理内存对齐和单位转换
class SizeClass
{
public:
	// 位运算写法
	// bytes：字节数		alignNum：对齐数
	static inline size_t _RoundUp(size_t bytes, size_t alignNum)
	{
		return ((bytes + alignNum - 1) & ~(alignNum - 1));
	}

	// 获取向上对齐后的字节数
	static inline size_t RoundUp(size_t bytes)
	{
		if (bytes <= 128)
		{
			return _RoundUp(bytes, 8);
		}
		else if (bytes <= 1024)
		{
			return _RoundUp(bytes, 16);
		}
		else if (bytes <= 8 * 1024)
		{
			return _RoundUp(bytes, 128);
		}
		else if (bytes <= 64 * 1024)
		{
			return _RoundUp(bytes, 1024);
		}
		else if (bytes <= 256 * 1024)
		{
			return _RoundUp(bytes, 8 * 1024);
		}
		else // 大于256KB的按页(8KB)对齐
		{
			return _RoundUp(bytes, 1 << PAGE_SHIFT);
		}
	}

	// 位运算写法
	static inline size_t _Index(size_t bytes, size_t alignShift)
	{
		return ((bytes + (1 << alignShift) - 1) >> alignShift) - 1;
	}

	// 从字节数转换为哈希桶的下标
	static inline size_t Index(size_t bytes)
	{
		// 每个字节范围有多少个自由链表
		static size_t groupArray[4] = { 16, 56, 56, 56 };
		if (bytes <= 128)
		{
			return _Index(bytes, 3);
		}
		else if (bytes <= 1024)
		{
			return _Index(bytes - 128, 4) + groupArray[0];
		}
		else if (bytes <= 8 * 1024)
		{
			return _Index(bytes - 1024, 7) + groupArray[0] + groupArray[1];
		}
		else if (bytes <= 64 * 1024)
		{
			return _Index(bytes - 8 * 1024, 10) + groupArray[0] + groupArray[1] + groupArray[2];
		}
		else if (bytes <= 256 * 1024)
		{
			return _Index(bytes - 64 * 1024, 13) + groupArray[0] + groupArray[1] + groupArray[2] + groupArray[3];
		}
		else
		{
			assert(false);
			return -1;
		}
	}
	// 返回ThreadCache向CentralCache获取Object的个数
	// objSize：单个对象的字节数
	static size_t NumMoveSize(size_t objBytes)
	{
		assert(objBytes > 0);
		// ThreadCache(空)最多能获取的对象个数
		int num = TC_MAX_BYTES / objBytes;

		if (num < 2) // 对象大，分少一点
		{
			num = 2;
		}
		else if (num > 512) // 对象小，分多一点
		{
			num = 512;
		}
		return num;
	}
	// 返回CentralCache向PageHeap申请的页数
	// bytes: ThreadCache向CentralCache申请Object的字节数
	static size_t NumMovePage(size_t objBytes)
	{
		size_t num = NumMoveSize(objBytes); // Object个数
		size_t size = num * objBytes; // 一次性申请的最大字节数

		size_t nPage = size >> PAGE_SHIFT;
		if (nPage == 0)
		{
			nPage = 1; // 至少给1页
		}
		return nPage;
	}
};

// Span：双向链表的结点，管理一个以页为单位的自由链表(Objets)
struct Span
{
	PAGE_ID _pageId = 0; // 页号，描述Span的起始位置
	size_t _nPage = 0; // Span中的页数

	Span* _prev = nullptr; // 前后结点指针
	Span* _next = nullptr;
	void* _objectsList = nullptr; // 指向由未被分配的Object组成的链表

	size_t _objSize = 0; // Span中Object的大小
	size_t _usedCount = 0; // 记录分配给ThreadCache的Object个数

	bool _isUsed = false; // 标记Span是否正在被线程访问
};
#include "ObjectPool.h"

// SpanList：双向链表，管理若干相同规格的Span
class SpanList
{
public:
	SpanList()
	{
		_head = _spanPool.New();
		_head->_next = _head;
		_head->_prev = _head;
	}
	void PushFront(Span* span)
	{
		Insert(Begin(), span);
	}
	Span* PopFront()
	{
		Span* front = _head->_next;
		Erase(front);
		return front;
	}
	Span* Begin()
	{
		return _head->_next;
	}
	Span* End()
	{
		return _head;
	}
	bool Empty()
	{
		return _head == _head->_next;
	}
	void Insert(Span* pos, Span* newSpan)
	{
		assert(pos);
		assert(newSpan);

		Span* prev = pos->_prev;

		prev->_next = newSpan;
		newSpan->_prev = prev;

		newSpan->_next = pos;
		pos->_prev = newSpan;
	}
	void Erase(Span* pos)
	{
		assert(pos);
		assert(pos != _head); // 不能删除哨兵位的头结点

		Span* prev = pos->_prev;
		Span* next = pos->_next;

		prev->_next = next;
		next->_prev = prev;
	}
	void Lock()
	{
		_mtx.lock();
	}
	void UnLock()
	{
		_mtx.unlock();
	}
	~SpanList() {}
public:
	Span* _head;
	std::mutex _mtx; // 桶锁
	static ObjectPool<Span> _spanPool; // Span池
};