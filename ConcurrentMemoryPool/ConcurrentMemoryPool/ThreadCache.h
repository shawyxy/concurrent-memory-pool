#pragma once

#include "Common.h"

class ThreadCache
{
public:
	// 分配内存
	void* Allocate(size_t bytes);

	// 回收内存
	void Deallocate(void* ptr, size_t bytes);

	// 从CentralCache中获取内存
	void* FetchFromCentralCache(size_t index, size_t bytes);

	// 当自由长度大于一次向CentralCache申请的个数，归还一部分
	void ListTooLong(FreeList& list, size_t bytes);

private:
	FreeList _freeLists[NUM_CLASSES]; // 哈希桶
};

// 用TLS声明线程管理ThreadCache资源的入口地址
static __declspec(thread) ThreadCache* TLSThreadCache_ptr = nullptr;