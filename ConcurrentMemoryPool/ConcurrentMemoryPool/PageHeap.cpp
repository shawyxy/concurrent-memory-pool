#include "PageHeap.h"

PageHeap PageHeap::_sInst;

// PageHeap取一个k页的Span给CentralCache
Span* PageHeap::NewSpan(size_t k)
{
	//assert(k > 0 && k < PH_MAX_PAGES); // 申请的有效页数
	assert(k > 0); // 增加了申请大于128页的逻辑

	// 大于128页
	if (k > PH_MAX_PAGES - 1)
	{
		void* ptr = SystemAlloc(k); // 向系统申请，得到地址
		//Span* kSpan = new Span;
		Span* kSpan = _spanPool.New();
		kSpan->_pageId = (PAGE_ID)ptr >> PAGE_SHIFT; // 将地址转换为页号
		kSpan->_nPage = k;

		//_idSpanMap[kSpan->_pageId] = kSpan;
		_idSpanMap.set(kSpan->_pageId, kSpan);

		return kSpan;
	}
	// 首先看自己有没有空闲的k页Span
	if (!_spanLists[k].Empty()) 
	{
		Span* kSpan = _spanLists[k].PopFront();

		// 建立`分配出去的`Span中的每个Page的页号和Span首地址之间的映射关系
		for (PAGE_ID i = 0; i < kSpan->_nPage; i++)
		{
			//_idSpanMap[kSpan->_pageId + i] = kSpan;
			_idSpanMap.set(kSpan->_pageId + i, kSpan);
		}

		return kSpan;
	}
	else // 没有k页Span，从k+1号桶往后找n页Span切分
	{
		for (int i = k + 1; i < PH_MAX_PAGES; i++)
		{
			if (!_spanLists[i].Empty())
			{
				Span* nSpan = _spanLists[i].PopFront();
				//Span* kSpan = new Span;
				Span* kSpan = _spanPool.New();

				// 将n页Span的前k页切下来，并更新两者的页号和页数
				kSpan->_pageId = nSpan->_pageId;
				kSpan->_nPage = k;

				nSpan->_pageId += k;
				nSpan->_nPage -= k;
				//将剩下的n-k页挂到桶中
				_spanLists[nSpan->_nPage].PushFront(nSpan);

				// 建立`未分配出去的`Span的首地址和`首尾Page页号`之间的映射关系(方便合并)
				//_idSpanMap[nSpan->_pageId] = nSpan;
				//_idSpanMap[nSpan->_pageId + nSpan->_nPage - 1] = nSpan;
				_idSpanMap.set(nSpan->_pageId, nSpan);
				_idSpanMap.set(nSpan->_pageId + nSpan->_nPage - 1, nSpan);


				// 建立`分配出去的`Span的首地址和`每个Page页号`之间的映射关系(方便回收时Object找到Span)
				for (PAGE_ID i = 0; i < kSpan->_nPage; i++)
				{
					//_idSpanMap[kSpan->_pageId + i] = kSpan;
					_idSpanMap.set(kSpan->_pageId + i, kSpan);
				}

				return kSpan;
			}
		}
	}
	// 一个大于k页的Span都没有
	// 向系统申请一个128页的Span
	void* ptr = SystemAlloc(PH_MAX_PAGES - 1);
	//Span* newSpan = new Span; // 用一个新Span管理新内存
	Span* newSpan = _spanPool.New();


	// 更新页号和页数
	newSpan->_pageId = (PAGE_ID)ptr >> PAGE_SHIFT;
	newSpan->_nPage = PH_MAX_PAGES - 1;
	// 将新Span挂到128号桶上
	_spanLists[newSpan->_nPage].PushFront(newSpan);

	// 递归调用，返回k页Span
	return NewSpan(k);
}
// 返回从Object地址到Span首地址的映射
Span* PageHeap::MapObjectToSpan(void* objAdr)
{
	PAGE_ID id = (PAGE_ID)objAdr >> PAGE_SHIFT; // 将地址转换为页号
	//std::unique_lock<std::mutex> lock(_pageMtx); // 构造时加锁，析构时自动解锁(有了基数树后不用加锁)
	//std::unordered_map<PAGE_ID, Span*>::iterator it = _idSpanMap.find(id); // 查表
	void* ptr = _idSpanMap.get(id); // 返回Span的指针
	if (ptr != nullptr)
	{
		return (Span*)ptr;
	}
	else
	{
		assert(false); // 没有映射一定有错误
		return nullptr;	// 仅为通过编译，不会执行
	}
}
// PageHeap回收空闲的Span，合并相邻的Span
void PageHeap::ReleaseSpanToPageHeap(Span* span)
{
	// 大于128页还给系统并释放空间
	if (span->_nPage > PH_MAX_PAGES - 1) 
	{
		void* ptr = (void*)(span->_pageId << PAGE_SHIFT); // 页号转换为地址
		SystemFree(ptr);
		//delete span;
		_spanPool.Delete(span);

		return;
	}
	
	// 向前合并
	while (true)
	{
		PAGE_ID preId = span->_pageId - 1; // Span左边的页号
		//std::unordered_map<PAGE_ID, Span*>::iterator it = _idSpanMap.find(preId); // 查表
		void* ptr = _idSpanMap.get(preId);
		if (ptr == nullptr) // 前面没有相邻的Span
		{
			break;
		}

		Span* preSpan = (Span*)ptr;
		if (preSpan->_isUsed == true) // 正在被CentralCache使用
		{
			break;
		}

		if (preSpan->_nPage + span->_nPage > PH_MAX_PAGES - 1) // 合并后大于128页
		{
			break;
		}

		// 向前合并，更新信息
		span->_nPage += preSpan->_nPage;
		span->_pageId = preSpan->_pageId;

		// 从桶中删除preSpan并其释放空间
		_spanLists[preSpan->_nPage].Erase(preSpan);
		//delete preSpan;
		_spanPool.Delete(preSpan);
	}

	// 向后合并
	while (true)
	{
		PAGE_ID nextId = span->_pageId + span->_nPage; // Span右边的页号
		//std::unordered_map<PAGE_ID, Span*>::iterator it = _idSpanMap.find(nextId); // 查表
		void* ptr = _idSpanMap.get(nextId);
		if (ptr == nullptr) // 后面没有相邻的Span
		{
			break;
		}

		Span* nextSpan = (Span*)ptr;
		if (nextSpan->_isUsed == true) // 正在被CentralCache使用
		{
			break;
		}

		if (nextSpan->_nPage + span->_nPage > PH_MAX_PAGES - 1) // 合并后大于128页
		{
			break;
		}

		// 向后直接合并，只更新页数
		span->_nPage += nextSpan->_nPage;

		// 从桶中删除nextSpan并其释放空间
		_spanLists[nextSpan->_nPage].Erase(nextSpan);
		//delete nextSpan;
		_spanPool.Delete(nextSpan);
	}

	// 将合并后的新Span挂到桶上，并标记为空闲
	_spanLists[span->_nPage].PushFront(span);
	span->_isUsed = false;

	// 建立新Span地址和首尾页号的映射关系，以方便它后续被合并
	//_idSpanMap[span->_pageId] = span;
	//_idSpanMap[span->_pageId + span->_nPage - 1] = span;
	_idSpanMap.set(span->_pageId, span);
	_idSpanMap.set(span->_pageId + span->_nPage - 1, span);
}