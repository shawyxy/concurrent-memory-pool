//#include "ObjectPool.h"
#include "ConcurrentAlloc.h"

// ObjectPool Test

struct Node
{
	int _val;
	Node(int val = 0)
		:_val(val)
	{}
};
void ObjectPoolTest()
{
	int Round = 100; // 申请/释放的轮次
	int N = 100000; // 每轮申请/释放次数
	
	// malloc/free
	std::vector<Node*> v1;
	v1.reserve(N);
	time_t begin1 = clock();
	for (int i = 0; i < Round; i++)
	{
		for (int j = 0; j < N; j++)
		{
			v1.push_back(new Node(1));
		}
		for (int j = 0; j < N; j++)
		{
			delete v1[j];
		}
		v1.clear();
	}
	time_t end1 = clock();
	// ObjectPool
	ObjectPool<Node> NodePool;
	std::vector<Node*> v2;
	v2.reserve(N);
	time_t begin2 = clock();
	for (int i = 0; i < Round; i++)
	{
		for (int j = 0; j < N; j++)
		{
			v2.push_back(NodePool.New());
		}
		for (int j = 0; j < N; j++)
		{
			NodePool.Delete(v2[j]);
		}
		v2.clear();
	}
	time_t end2 = clock();
	std::cout << "malloc/free cost time: " << end1 - begin1 << "ms" << std::endl;
	std::cout << "Object Pool cost time: " << end2 - begin2 << "ms" << std::endl;
}
// TLS Test
void Alloc1()
{
	for (int i = 0; i < 5; i++)
	{
		void* ptr = ConcurrentAlloc(5);
	}
}
void Alloc2()
{
	for (int i = 0; i < 4; i++)
	{
		void* ptr = ConcurrentAlloc(4);
	}
}
void TLSTest()
{
	std::thread t1(Alloc1);
	std::thread t2(Alloc2);

	t1.join();
	t2.join();
}

// 测试对齐
void AlignTest1()
{
	void* ptr1 = ConcurrentAlloc(6);
	void* ptr2 = ConcurrentAlloc(8);
	void* ptr3 = ConcurrentAlloc(10);

	std::cout << ptr1 << std::endl;
	std::cout << ptr2 << std::endl;
	std::cout << ptr3 << std::endl;
}
void AlignTest2()
{
	for (int i = 0; i < 1024; i++)
	{
		void* ptr = ConcurrentAlloc(8);
		std::cout << ptr << std::endl;
	}
	void* ptr = ConcurrentAlloc(8);
	std::cout << ptr << std::endl;
}
// 测试释放内存
void ConcurrentFreeTest()
{
	void* ptr1 = ConcurrentAlloc(6);
	void* ptr2 = ConcurrentAlloc(8);
	void* ptr3 = ConcurrentAlloc(10);
	void* ptr4 = ConcurrentAlloc(17);
	void* ptr5 = ConcurrentAlloc(20);

	//ConcurrentFree(ptr1, 6);
	//ConcurrentFree(ptr2, 8);
	//ConcurrentFree(ptr3, 10);
	//ConcurrentFree(ptr4, 17);
	//ConcurrentFree(ptr5, 20);
}
// 多线程申请和释放
void MultiThreadAlloc1()
{
	std::vector<void*> v;
	for (size_t i = 0; i < 7; ++i) // 申请7次，正好单个线程能走到pc回收cc中span的那一步
	{
		void* ptr = ConcurrentAlloc(6); // 申请的都是8B的块空间
		v.push_back(ptr);
	}

	for (auto e : v)
	{
		ConcurrentFree(e);
	}
}

void MultiThreadAlloc2()
{
	std::vector<void*> v;
	for (size_t i = 0; i < 7; ++i)
	{
		void* ptr = ConcurrentAlloc(16); // 申请的都是16B的块空间
		v.push_back(ptr);
	}

	for (int i = 0; i < 7; ++i)
	{
		ConcurrentFree(v[i]);
	}
}

void TestMultiThread()
{
	std::thread t1(MultiThreadAlloc1);
	std::thread t2(MultiThreadAlloc2);

	t1.join();
	t2.join();
}
 //大内存申请和释放测试
void BigAllocTest()
{
	// 找PageHeap申请
	void* ptr1 = ConcurrentAlloc(257 * 1024); // 257KB
	ConcurrentFree(ptr1);

	// 找堆申请
	void* ptr2 = ConcurrentAlloc(129 * 8 * 1024); // 129页
	ConcurrentFree(ptr2);
}

//int main()
//{
//	//ObjectPoolTest();
//	//TLSTest();
//	//AlignTest1();
//	//AlignTest2();
//	//ConcurrentFreeTest();
//	//TestMultiThread();
//	//BigAllocTest();
//
//	return 0;
//}